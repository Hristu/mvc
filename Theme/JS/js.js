function ajax_post(account) {
    var xmlhttp = new XMLHttpRequest();
    var userName =  document.getElementById("userName").value;
    var fName = document.getElementById("fName").value;
    var lName = document.getElementById("lName").value;
    var email = document.getElementById("email").value;
    var vars = new AccountData();
    vars.append("userName", userName);
    vars.append("fName", fName);
    vars.append("lName", lName);
    vars.append("email", email);
    xmlhttp.open("POST", "Views/AccountView.php", true);
    xmlhttp.send(vars);
    return false;
}