<?php
class Connection
{
    public $mysqli;
    private static $instance;


   public static function getInstance()
    {
        if(!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

   public function connection()
    {
       /* Attempt to connect to MySQL database */
        $this->mysqli = new mysqli('localhost', 'root', '', 'testData');
        // Check connection
        if($this->mysqli === false){
            die("ERROR: Could not connect. " . $this->mysqli->connect_error);
        }
    }
    public function executeQuery($sql)
    {
        $result = $this->mysqli->query($sql);
        return $result;
    }

    public function __construct()
    {
        $this->connection();
    }

}


