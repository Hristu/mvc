
<?php
require_once 'Connection.php';

class BaseModel 
{
        private $connection;

        public function __construct()
        {
                $this->connection = Connection::getinstance();
        }

        public function executeQuery($query)
        {
                $result = $this->connection -> executeQuery($query); // aici
                return $result;
        }
}

