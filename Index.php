<?php
 require_once 'Models/Instal.php';

session_start();
$instal = new Instal();
$sql = $instal->createUsers();
$currentLink = $_GET['route'];

$test = explode('/', $currentLink);
if ($test[0]== 'account' && $test[1]=='login') {
    require 'Controllers/LoginControl.php';
    $controller = new LoginControl();
    $controller->login();
} elseif($test[0]== 'account' && $test[1]== 'register') {
    require 'Controllers/RegisterControl.php';
    $controller = new RegisterControl();
    $controller->register();
} elseif($test[0]== 'account' && $test[1]== 'welcome') {
    require 'Controllers/AccountControl.php';
    $controller = new AccountControl();
    $controller->welcome();
}
// } elseif($test[0] == 'account' && $test[1] == 'loginPost') {
//     require 'Controllers/LoginControl.php';
//     $controller = new LoginControl();
//     $controller->login();
// }