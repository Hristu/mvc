<?php
require 'Views/LoginView.php';
require 'Models/User.php';
class LoginControl
{

    public function login()
    {
        if($_SERVER["REQUEST_METHOD"] == "POST") {
            $userName = trim($_POST["userName"]);
            $password = trim($_POST["password"]);

            $userData = array(
                'userName' => $userName,
                'password' => $password
            );

            $userModel = new User();
            $loginMessage = $userModel->Login($userData);

            if($loginMessage != false) {
                $_SESSION['id'] = $loginMessage;
                header("location:Index.php?route=account/welcome");
            } else {
                echo "no account with this username or user and pass wrong";
            }
        } else {
            $loginView = new LoginView();
            $loginView -> Index();
        }
    }

}




