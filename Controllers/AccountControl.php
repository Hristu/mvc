<?php

session_start();

require_once "Views/AccountView.php";
require_once "LoginControl.php";

class AccountControl
{

    public function welcome()
    {
    $loggedInUserId = $_SESSION['id'];

    $userModel = new User();
    $userMessage = $userModel->loadFromDb($loggedInUserId);

    $accountView = new AccountView();
    $accountView->Index($userMessage);

    }
}






