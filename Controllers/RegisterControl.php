<?php

require 'Views/RegisterView.php';
require 'Models/User.php';

class RegisterControl
{


    public function register()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            $userName = trim($_POST['userName']);
            $firstName = trim($_POST['firstName']);
            $lastName = trim($_POST['lastName']);
            $email = trim($_POST['email']);
            $password = trim($_POST['password']);
            $passwordConfirmation = trim($_POST['passwordConfirmation']);

            $userDetails = array(
                'username' => $userName,
                'firstname' => $firstName,
                'lastName' => $lastName,
                'email' => $email,
                'password' => $password,
                'passwordConfirmation' => $passwordConfirmation
            );

            $userModel = new User();
            $registerMessage = $userModel->register($userDetails);
            echo $registerMessage;

        } else {
        echo 'To only method that is allowed is POST';
    }
 


    $registerView = new RegisterView();
    $registerView->Index();
    }

}






